<?php

/**
 *
 */
class Admin extends CI_Controller{

  public function __construct() {
    parent::__construct();
    if ($this->session->userdata('jabatan')!="Admin") {
      redirect('auth');
    }
    $this->load->model('admin/upgrade_semester_m');
    $this->load->helper('text');
  }

  public function index() {
		$this->load->view('admin/login');
	}

  public function dashboard() {
		$this->load->view('templates/header');
		$this->load->view('admin/landingpage');
    $this->load->view('templates/footer');
	}

  public function logout() {
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('jabatan');
			session_destroy();
			redirect(base_url(''));
		}

    public function upgrade_semester(){
      $this->upgrade_semester_m->upgrade();
    }


}

 ?>
