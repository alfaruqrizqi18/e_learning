<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Akun_Dsn extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('jabatan')=="") {
      redirect('auth');
    }
		$this->load->model('admin/akun_m_dsn');
		$this->load->helper('url_helper');
	}

	public function index(){
		$db['dataakun'] = $this->akun_m_dsn->get_data();
		//me load model akun_m dengan function get_data
		$this->load->view('templates/header');
		$this->load->view('admin/akun_v_dsn', $db);
		$this->load->view('templates/footer');
	}

	public function create(){
		$this->load->helper('form');
		$this->load->library('form_validation');
    $this->load->helper('form_helper');
 		$data = $this->akun_m_dsn->data_dsn();

		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() === FALSE) {

		$this->load->view('templates/header');
		$this->load->view('admin/add_akun_dsn', array('data' => $data));
		$this->load->view('templates/footer');
		}else{
    $id = $this->input->post('nama');
		$this->akun_m_dsn->set_data();
    $this->akun_m_dsn->update_status_for_set($id);
		redirect('akun_dsn');
		}
	}

	public function update($id){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form_helper');

    $this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() === FALSE) {
		$data1 = $this->akun_m_dsn->detail_data($id);
		$data2 = $this->akun_m_dsn->data_dsn();
		$this->load->view('templates/header');
		$this->load->view('admin/update_akun_dsn', array('data1' => $data1, 'data2' => $data2));
		$this->load->view('templates/footer');
		}else{
		$this->akun_m_dsn->update_data($id);
		redirect('akun_dsn');
		}
	}

	public function delete($id,$id2){
    $this->akun_m_dsn->update_status_for_delete($id2);
		$this->akun_m_dsn->delete_data($id);
		redirect('akun_dsn');
	}

}
 ?>
