<?php

/**
 *
 */
class Dashboard extends CI_Controller{

  public function __construct() {
    parent::__construct();
    if ($this->session->userdata('jabatan')=="") {
      redirect('authentification');
    }
    $this->load->helper('text');
    $this->load->model('dosen/dosen_makul_m');
    $this->load->model('mahasiswa/mahasiswa_makul_m');
    $this->load->model('mailbox/mailbox_m');
  }

  public function index() {
		$this->load->view('login');
	}
  public function dosen() {
    $db['dataakun'] = $this->dosen_makul_m->get_data();
    $db['datadsn'] = $this->dosen_makul_m->get_data();
    $db['datamakulpengajar'] = $this->dosen_makul_m->get_data();
    $db['datamailbox'] = $this->mailbox_m->get_data();
    $db['data_unread'] = $this->mailbox_m->count_get_data_unread();
		$this->load->view('templates-dosen/header',$db);
		$this->load->view('dosen/index');
    $this->load->view('templates-dosen/footer');
	}
  public function mahasiswa() {
    $db['datamhs'] = $this->mahasiswa_makul_m->get_data();
    $db['datamakul'] = $this->mahasiswa_makul_m->get_data();
    $db['datamailbox'] = $this->mailbox_m->get_data();
    $db['data_unread'] = $this->mailbox_m->count_get_data_unread();
		$this->load->view('templates-mahasiswa/header',$db);
		$this->load->view('mahasiswa/index');
    $this->load->view('templates-mahasiswa/footer');
	}
  public function logout() {
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('jabatan');
			session_destroy();
			redirect(base_url(''));
		}
}

 ?>
