<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Dosen extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('jabatan')=="") {
      redirect('auth');
    }
		$this->load->model('admin/dosen_m');
		$this->load->helper('url_helper');
	}

	public function index(){
		$db['datadsn'] = $this->dosen_m->get_data();
		//me load model dosen_m dengan function get_data

		$this->load->view('templates/header');
		$this->load->view('admin/dosen_v', $db);
		$this->load->view('templates/footer');
	}

	public function create(){
		$this->load->helper('form');
		$this->load->library('form_validation');
    $this->load->helper('form_helper');
 		$data = $this->dosen_m->data_jurusan();

		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('kelamin', 'Kelamin', 'required');
		$this->form_validation->set_rules('tempatlahir', 'Tempat Lahir', 'required');
		$this->form_validation->set_rules('tanggallahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required');

		if ($this->form_validation->run() === FALSE) {

		$this->load->view('templates/header');
		$this->load->view('admin/add_dosen', array('data' => $data));
		$this->load->view('templates/footer');
		}else{
		$this->dosen_m->set_data();
		redirect('dosen');
		}
	}

	public function update($id){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form_helper');

        $this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('kelamin', 'Kelamin', 'required');
		$this->form_validation->set_rules('tempatlahir', 'Tempat Lahir', 'required');
		$this->form_validation->set_rules('tanggallahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required');

		if ($this->form_validation->run() === FALSE) {
		$data1 = $this->dosen_m->detail_data($id);
		$data2 = $this->dosen_m->data_jurusan();
		$this->load->view('templates/header');
		$this->load->view('admin/update_dosen', array('data1' => $data1, 'data2' => $data2));
		$this->load->view('templates/footer');
		}else{
		$this->dosen_m->update_data($id);
		redirect('dosen');
		}
	}

	public function delete($id){
		$this->dosen_m->delete_data($id);
		redirect('dosen');
	}

}
 ?>
