<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Jurusan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('jabatan')=="") {
      redirect('auth');
    }
		$this->load->model('admin/jurusan_m');
		$this->load->helper('url_helper');
	}

	public function index(){
		$db['datajurusan'] = $this->jurusan_m->get_data();
		//me load model jurusan_m dengan function get_data

		$this->load->view('templates/header');
		$this->load->view('admin/jurusan_v', $db);
		$this->load->view('templates/footer');
	}

	public function create(){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');

		if ($this->form_validation->run() === FALSE) {

		$this->load->view('templates/header');
		$this->load->view('admin/add_jurusan');
		$this->load->view('templates/footer');
		}else{
		$this->jurusan_m->set_data();
		redirect('jurusan');
		}
	}

	public function update($id){
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama', 'Nama', 'required');

		if ($this->form_validation->run() === FALSE) {
		$data['data_item'] = $this->jurusan_m->get_data_id($id);
		$this->load->view('templates/header');
		$this->load->view('admin/update_jurusan', $data);
		$this->load->view('templates/footer');
		}else{
		$this->jurusan_m->update_data($id);
		redirect('jurusan');
		}
	}

	public function delete($id){
		$this->jurusan_m->delete_data($id);
		redirect('jurusan');
	}
}
 ?>
