<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Mahasiswa extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('jabatan')=="") {
      redirect('auth');
    }
		$this->load->model('admin/mahasiswa_m');
		$this->load->helper('url_helper');
	}

	public function index(){
		$db['datamahasiswa'] = $this->mahasiswa_m->get_data();
		//me load model mahasiswa_m dengan function get_data

		$this->load->view('templates/header');
		$this->load->view('admin/mahasiswa_v', $db);
		$this->load->view('templates/footer');
	}

	public function create(){
		$this->load->helper('form');
		$this->load->library('form_validation');
    $this->load->helper('form_helper');
 		$data = $this->mahasiswa_m->data_jurusan();

		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('kelamin', 'Kelamin', 'required');
		$this->form_validation->set_rules('tempatlahir', 'Tempat Lahir', 'required');
		$this->form_validation->set_rules('tanggallahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required');
		$this->form_validation->set_rules('semester', 'Semester', 'required');
		$this->form_validation->set_rules('kelas', 'Kelas', 'required');

		if ($this->form_validation->run() === FALSE) {

		$this->load->view('templates/header');
		$this->load->view('admin/add_mahasiswa', array('data' => $data));
		$this->load->view('templates/footer');
		}else{
		$this->mahasiswa_m->set_data();
		redirect('mahasiswa');
		}
	}

	public function update($id){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form_helper');

        $this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('kelamin', 'Kelamin', 'required');
		$this->form_validation->set_rules('tempatlahir', 'Tempat Lahir', 'required');
		$this->form_validation->set_rules('tanggallahir', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required');
		$this->form_validation->set_rules('semester', 'Semester', 'required');
		$this->form_validation->set_rules('kelas', 'Kelas', 'required');

		if ($this->form_validation->run() === FALSE) {
		$data1 = $this->mahasiswa_m->detail_data($id);
		$data2 = $this->mahasiswa_m->data_jurusan();
		$this->load->view('templates/header');
		$this->load->view('admin/update_mahasiswa', array('data1' => $data1, 'data2' => $data2));
		$this->load->view('templates/footer');
		}else{
		$this->mahasiswa_m->update_data($id);
		redirect('mahasiswa');
		}
	}

	public function delete($id){
		$this->mahasiswa_m->delete_data($id);
		redirect('mahasiswa');
	}

}
 ?>
