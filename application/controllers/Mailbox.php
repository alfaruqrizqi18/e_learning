<?php

/**
 *
 */
class Mailbox extends CI_Controller{

  function __construct(){
    parent::__construct();
    if ($this->session->userdata('jabatan')=="") {
      redirect('authentification');
    }
    $this->load->model('dosen/dosen_makul_m');
    $this->load->model('mahasiswa/mahasiswa_makul_m');
    $this->load->model('mailbox/mailbox_m');
  }

  public function index(){
    if ($this->session->userdata('jabatan')=="Dosen") {
      $db['dataakun'] = $this->dosen_makul_m->get_data();
      $db['datadsn'] = $this->dosen_makul_m->get_data();
      $db['datamakulpengajar'] = $this->dosen_makul_m->get_data();
      $db['datamailbox'] = $this->mailbox_m->get_data();
      $db['data_unread'] = $this->mailbox_m->count_get_data_unread();
  		$this->load->view('templates-dosen/header',$db);
  		$this->load->view('dosen/mailbox',$db);
      $this->load->view('templates-dosen/footer');

    }else if($this->session->userdata('jabatan')=="Mahasiswa"){

      $db['datamhs'] = $this->mahasiswa_makul_m->get_data();
      $db['datamakul'] = $this->mahasiswa_makul_m->get_data();
      $db['datamailbox'] = $this->mailbox_m->get_data();
      $db['data_unread'] = $this->mailbox_m->count_get_data_unread();
  		$this->load->view('templates-mahasiswa/header',$db);
  		$this->load->view('mahasiswa/mailbox',$db);
      $this->load->view('templates-mahasiswa/footer');
    }

  }

//START CREATE///
  public function create(){
    $this->load->helper('form');
		$this->load->library('form_validation');
    $this->load->helper('form_helper');

		$this->form_validation->set_rules('penerima', 'Penerima', 'required');
    $this->form_validation->set_rules('judulpesan', 'Judul Pesan', 'required');
		$this->form_validation->set_rules('isipesan', 'Isi Pesan', 'required');

    if ($this->session->userdata('jabatan')=="Dosen") {
      if ($this->form_validation->run() === FALSE) {

      $data = $this->mailbox_m->data_mahasiswa();
      $db['datamailbox'] = $this->mailbox_m->get_data();
      $db['data_unread'] = $this->mailbox_m->count_get_data_unread();
  		$this->load->view('templates-dosen/header',$db);
  		$this->load->view('dosen/add_pesan', array('data' => $data));
  		$this->load->view('templates-dosen/footer');
  		}else{
  		$this->mailbox_m->set_data();
  		redirect('mailbox');
  		}
    }elseif ($this->session->userdata('jabatan')=="Mahasiswa") {

      if ($this->form_validation->run() === FALSE) {

   		$data = $this->mailbox_m->data_dosen();
      $db['datamhs'] = $this->mahasiswa_makul_m->get_data();
      $db['datamakul'] = $this->mahasiswa_makul_m->get_data();
      $db['datamailbox'] = $this->mailbox_m->get_data();
      $db['data_unread'] = $this->mailbox_m->count_get_data_unread();
  		$this->load->view('templates-mahasiswa/header',$db);
  		$this->load->view('mahasiswa/add_pesan', array('data' => $data));
  		$this->load->view('templates-mahasiswa/footer');
  		}else{
  		$this->mailbox_m->set_data();
  		redirect('mailbox');
  		}
    }
  }
//FINISH CREATE///

  public function detail($id){
    $this->load->helper('form');
		$this->load->library('form_validation');
    $this->mailbox_m->update_status($id);
    $db['data_unread'] = $this->mailbox_m->count_get_data_unread();
    $db['detail_mail'] = $this->mailbox_m->show_detail_mail($id);
      if ($this->session->userdata('jabatan')=="Dosen") {
        $db['dataakun'] = $this->dosen_makul_m->get_data();
        $db['datadsn'] = $this->dosen_makul_m->get_data();
        $db['datamakulpengajar'] = $this->dosen_makul_m->get_data();
        $db['datamailbox'] = $this->mailbox_m->get_data();
        $this->load->view('templates-dosen/header',$db);
        $this->load->view('dosen/view_mail', $db);
        $this->load->view('templates-dosen/footer');
      } else if($this->session->userdata('jabatan')=="Mahasiswa"){
        $db['datamhs'] = $this->mahasiswa_makul_m->get_data();
        $db['datamakul'] = $this->mahasiswa_makul_m->get_data();
        $db['datamailbox'] = $this->mailbox_m->get_data();
        $this->load->view('templates-mahasiswa/header',$db);
        $this->load->view('mahasiswa/view_mail', $db);
        $this->load->view('templates-mahasiswa/footer');
      }
		}

    public function reply($id){
      $this->load->helper('form');
  		$this->load->library('form_validation');
      $db['datamailbox'] = $this->mailbox_m->get_data();
      $db['data_unread'] = $this->mailbox_m->count_get_data_unread();

      $this->form_validation->set_rules('judulpesan', 'Judul Pesan', 'required');
  		$this->form_validation->set_rules('isipesan', 'Isi Pesan', 'required');

        if ($this->session->userdata('jabatan')=="Dosen") {
          if ($this->form_validation->run() === FALSE) {
          $db['dataakun'] = $this->dosen_makul_m->get_data();
          $db['datadsn'] = $this->dosen_makul_m->get_data();
          $db['datamakulpengajar'] = $this->dosen_makul_m->get_data();
          $db['data_mahasiswa'] = $this->mailbox_m->data_mahasiswa_get_id_reply($id);
          $this->load->view('templates-dosen/header',$db);
          $this->load->view('dosen/reply', $db);
          $this->load->view('templates-dosen/footer');
        } else {
          $this->mailbox_m->reply($id);
      		redirect('mailbox');
              }
        } else if($this->session->userdata('jabatan')=="Mahasiswa"){
          if ($this->form_validation->run() === FALSE) {
          $db['datamhs'] = $this->mahasiswa_makul_m->get_data();
          $db['datamakul'] = $this->mahasiswa_makul_m->get_data();
          $db['data_dosen'] = $this->mailbox_m->data_dosen_get_id_reply($id);
          $this->load->view('templates-mahasiswa/header',$db);
          $this->load->view('mahasiswa/reply', $db);
          $this->load->view('templates-mahasiswa/footer');
        } else {
          $this->mailbox_m->reply($id);
          redirect('mailbox');
        }
      }
  }

    public function delete($id){
  		$this->mailbox_m->delete_data($id);
  		redirect('mailbox');
  	}




  }



 ?>
