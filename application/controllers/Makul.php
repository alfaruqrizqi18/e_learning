<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Makul extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('jabatan')=="") {
      redirect('auth');
    }
		$this->load->model('admin/makul_m');
		$this->load->helper('url_helper');
	}

	public function index(){
		$db['datamakul'] = $this->makul_m->get_data();
		//me load model makul_m dengan function get_data

		$this->load->view('templates/header');
		$this->load->view('admin/makul_v', $db);
		$this->load->view('templates/footer');
	}

	public function create(){
		$this->load->helper('form');
		$this->load->library('form_validation');
        $this->load->helper('form_helper');
 		$data = $this->makul_m->data_jurusan();

		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required');
		$this->form_validation->set_rules('semester', 'Semester', 'required');

		if ($this->form_validation->run() === FALSE) {

		$this->load->view('templates/header');
		$this->load->view('admin/add_makul', array('data' => $data));
		$this->load->view('templates/footer');
		}else{
		$this->makul_m->set_data();
		redirect('makul');
		}
	}

	public function update($id){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form_helper');

        $this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'required');
		$this->form_validation->set_rules('semester', 'Semester', 'required');

		if ($this->form_validation->run() === FALSE) {
		$data1 = $this->makul_m->detail_data($id);
		$data2 = $this->makul_m->data_jurusan();
		$this->load->view('templates/header');
		$this->load->view('admin/update_makul', array('data1' => $data1, 'data2' => $data2));
		$this->load->view('templates/footer');
		}else{
		$this->makul_m->update_data($id);
		redirect('makul');
		}
	}

	public function delete($id){
		$this->makul_m->delete_data($id);
		redirect('makul');
	}

}
 ?>
