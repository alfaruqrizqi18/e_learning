<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Makul_Pengajar extends CI_Controller {

	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('jabatan')=="") {
      redirect('auth');
    }
		$this->load->model('admin/makul_pengajar_m');
		$this->load->helper('url_helper');
	}

	public function index(){
		$db['datamakulpengajar'] = $this->makul_pengajar_m->get_data();
		//me load model akun_m dengan function get_data
		$this->load->view('templates/header');
		$this->load->view('admin/makul_pengajar_v', $db);
		$this->load->view('templates/footer');
	}

	public function create(){
		$this->load->helper('form');
		$this->load->library('form_validation');
    $this->load->helper('form_helper');
 		$data = $this->makul_pengajar_m->data_dsn();

		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('makul', 'Makul', 'required');

		if ($this->form_validation->run() === FALSE) {

		$this->load->view('templates/header');
		$this->load->view('admin/add_makul_pengajar', array('data' => $data));
		$this->load->view('templates/footer');
		}else{
    $nama = $this->input->post('nama');
    $makul = $this->input->post('makul');
        if ($nama == "0" && $makul == "0") {
          redirect('makul_pengajar/create');
        }else if($nama == "0"){
          redirect('makul_pengajar/create');
        }else if($makul == "0"){
          redirect('makul_pengajar/create');
        }else{
      		$this->makul_pengajar_m->set_data();
      		redirect('makul_pengajar');
        }
		}
	}

  function ambil_data(){
  $modul=$this->input->post('modul');
  $id=$this->input->post('id');

  if($modul=="makul"){
  echo $this->makul_pengajar_m->get_makul($id);
  }
  }

	public function update($id){
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('form_helper');

    $this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('makul', 'Makul', 'required');

		if ($this->form_validation->run() === FALSE) {
		$data1 = $this->makul_pengajar_m->detail_data($id);
		$data2 = $this->makul_pengajar_m->data_dsn();
		$this->load->view('templates/header');
		$this->load->view('admin/update_makul_pengajar', array('data1' => $data1, 'data2' => $data2));
		$this->load->view('templates/footer');
		}else{
    $this->makul_pengajar_m->update_data($id);
    redirect('makul_pengajar');
		}
	}

	public function delete($id){
		$this->makul_pengajar_m->delete_data($id);
		redirect('makul_pengajar');
	}

}
 ?>
