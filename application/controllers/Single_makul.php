<?php

/**
 *
 */
class Single_Makul extends CI_Controller{

  function __construct(){
    parent::__construct();
    if ($this->session->userdata('jabatan')=="") {
      redirect('authentification');
    }
    $this->load->helper('text');
    $this->load->model('dosen/dosen_makul_m');
    $this->load->model('mahasiswa/mahasiswa_makul_m');
    $this->load->model('mailbox/mailbox_m');
		$this->load->helper('url_helper');
      $this->load->helper('form');
  		$this->load->library('form_validation');
  }

  public function index($id) {
		if ($this->session->userdata('jabatan')=="Dosen") {
      $db['datamateri_count'] = $this->dosen_makul_m->show_count($id);
      $db['datamateri_list_materi'] = $this->dosen_makul_m->show_all_materi($id);
      $db['datamakulpengajar'] = $this->dosen_makul_m->get_data();
      $db['datamakul_id'] = $this->dosen_makul_m->get_id_makul($id);
      $db['show_nama_makul'] = $this->dosen_makul_m->show_nama_makul($id);
      $db['datamailbox'] = $this->mailbox_m->get_data();
      $db['data_unread'] = $this->mailbox_m->count_get_data_unread();
      $this->load->view('templates-dosen/header',$db);
      $this->load->view('dosen/single_makul',$db);
      $this->load->view('templates-dosen/footer');
		}else if ($this->session->userdata('jabatan')=="Mahasiswa"){
      $db['datamateri_count'] = $this->mahasiswa_makul_m->show_count($id);
      $db['datamateri_list_materi'] = $this->mahasiswa_makul_m->show_all_materi($id);
      $db['datamakul'] = $this->mahasiswa_makul_m->get_data();
      $db['datamailbox'] = $this->mailbox_m->get_data();
      $db['data_unread'] = $this->mailbox_m->count_get_data_unread();
      $db['show_nama_makul'] = $this->mahasiswa_makul_m->show_nama_makul($id);
      $this->load->view('templates-mahasiswa/header',$db);
      $this->load->view('mahasiswa/single_makul',$db);
      $this->load->view('templates-mahasiswa/footer');
    }
  }

  public function create($id){

		$this->form_validation->set_rules('nama', 'Nama', 'required');
    if (empty($_FILES['materi']['name'])){
    $this->form_validation->set_rules('materi', 'Materi', 'required');
      }

		if ($this->form_validation->run() === FALSE) {
    $db['datamakulpengajar'] = $this->dosen_makul_m->get_data();
    $db['datamakul_id'] = $this->dosen_makul_m->get_id_makul($id);
    $db['datamailbox'] = $this->mailbox_m->get_data();
    $db['data_unread'] = $this->mailbox_m->count_get_data_unread();
    $this->load->view('templates-dosen/header',$db);
		$this->load->view('dosen/add_materi',$id);
		$this->load->view('templates-dosen/footer');
		}else{
		$this->dosen_makul_m->set_data($id);
		redirect('single_makul/index/'.$id);
		}
  }

  public function delete($id,$id2){
    $this->dosen_makul_m->delete_data($id);
		redirect('single_makul/index/'.$id2);
  }




}


 ?>
