<?php

/**
*
*/
class Akun_m_dsn extends CI_Model{

	public function __construct(){
		$this->load->database();
	}


  public function get_data() {
		$query = $this->db->query(
				"SELECT dataakun.idakun, dataakun.namaakun, datadsn.namadsn, dataakun.username,
                dataakun.jabatan
         FROM dataakun, datadsn
         WHERE dataakun.namaakun = datadsn.iddsn
				 ORDER BY jabatan ASC");
		return $query->result_array();
	}

	public function detail_data($id) {
			$data = $this->db->query(
					"SELECT dataakun.idakun, dataakun.namaakun, datadsn.namadsn, dataakun.username,
                  dataakun.password, dataakun.jabatan
           FROM dataakun, datadsn
           WHERE dataakun.namaakun = datadsn.iddsn
           AND dataakun.idakun = '$id' ");
			return $data->result_array();
		}
	public function data_dsn() {
			$data = $this->db->query('SELECT * FROM datadsn WHERE status = 0');
			return $data->result_array();
		}
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
	public function set_data() {
		$this->load->helper('url');
		$data = array(
		//nama field dalam tabel 	//atribut nama dari view
			'namaakun' => $this->input->post('nama'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
      'jabatan' =>  $this->input->post('jabatan')
			);
					//nama tabel di database  // parameter array
		return $this->db->insert('dataakun', $data);
	}

  public function update_status_for_set($id){
		$this->load->helper('url');
		$data = array(
      'status' => "1"
			);
		$this->db->where('iddsn', $id);
						//nama tabel di database  // parameter array
		return $this->db->update('datadsn', $data);
	}


//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////


	public function update_data($id){
		$this->load->helper('url');
		$data = array(
      'namaakun' => $this->input->post('nama'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
      'jabatan' => $this->input->post('jabatan')
			);

		$this->db->where('idakun', $id);
						//nama tabel di database  // parameter array
		return $this->db->update('dataakun', $data);
	}

  //////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////

	public function delete_data($id){
		return $this->db->delete('dataakun', array('idakun' => $id));
	}

  public function update_status_for_delete($id){
		$this->load->helper('url');
		$data = array(
      'status' => "0"
			);

		$this->db->where('iddsn', $id);
						//nama tabel di database  // parameter array
		return $this->db->update('datadsn', $data);
	}




}
 ?>
