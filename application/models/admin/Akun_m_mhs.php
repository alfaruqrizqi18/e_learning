<?php

/**
*
*/
class Akun_m_mhs extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_data() {
		$query = $this->db->query(
				'SELECT dataakun.idakun, dataakun.namaakun, datamhs.namamhs, dataakun.username, dataakun.jabatan
         FROM dataakun, datamhs
         WHERE dataakun.namaakun = datamhs.idmhs');
		return $query->result_array();
	}



	public function detail_data($id) {
			$data = $this->db->query(
					"SELECT dataakun.idakun, dataakun.namaakun, datamhs.namamhs, dataakun.username,
                  dataakun.password, dataakun.jabatan
           FROM dataakun, datamhs
           WHERE dataakun.namaakun = datamhs.idmhs
           AND dataakun.idakun = '$id' ");
			return $data->result_array();
		}
	public function data_mhs() {
			$data = $this->db->query('SELECT * FROM datamhs WHERE status = 0');
			return $data->result_array();
		}
    //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////
	public function set_data() {
		$this->load->helper('url');
		$data = array(
		//nama field dalam tabel 	//atribut nama dari view
			'namaakun' => $this->input->post('nama'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
      'jabatan' => "Mahasiswa"
			);
					//nama tabel di database  // parameter array
		return $this->db->insert('dataakun', $data);
	}

  public function update_status_for_set($id){
		$this->load->helper('url');
		$data = array(
      'status' => "1"
			);
		$this->db->where('idmhs', $id);
						//nama tabel di database  // parameter array
		return $this->db->update('datamhs', $data);
	}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
	public function update_data($id){
		$this->load->helper('url');
		$data = array(
      'namaakun' => $this->input->post('nama'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
      'jabatan' => "Mahasiswa"
			);

		$this->db->where('idakun', $id);
						//nama tabel di database  // parameter array
		return $this->db->update('dataakun', $data);
	}
  //////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////
	public function delete_data($id){
		return $this->db->delete('dataakun', array('idakun' => $id));
	}

  public function update_status_for_delete($id){
		$this->load->helper('url');
		$data = array(
      'status' => "0"
			);

		$this->db->where('idmhs', $id);
						//nama tabel di database  // parameter array
		return $this->db->update('datamhs', $data);
	}




}
 ?>
