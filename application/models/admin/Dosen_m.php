<?php

/**
*
*/
class Dosen_m extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_data() {
		$query = $this->db->query(
				'SELECT datadsn.`iddsn`,datadsn.`namadsn`,datadsn.`genderdsn`,
						datadsn.`tempatlahirdsn`, date_format(datadsn.`tanggallahirdsn`,"%d %M %Y")
				 AS 	tanggallahirdsn, datadsn.`alamatdsn`,datajurusan.`namajurusan`
                 FROM datadsn,datajurusan
                 WHERE datadsn.`jurusandsn` = datajurusan.`idjurusan`');
		return $query->result_array();
	}

	public function detail_data($id) {
			$data = $this->db->query(
				"SELECT datadsn.`iddsn`,datadsn.`namadsn`,datadsn.`genderdsn`,
						datadsn.`tempatlahirdsn`, datadsn.`tanggallahirdsn`,
                      	datadsn.`alamatdsn`,datajurusan.`namajurusan`,
                      	datadsn.`jurusandsn`
                 FROM datadsn,datajurusan
                 WHERE datadsn.`jurusandsn` = datajurusan.`idjurusan`
                 AND datadsn.`iddsn` = '$id' ");
			return $data->result_array();
		}

	public function data_jurusan() {
			$data = $this->db->query('select * from datajurusan');
			return $data->result_array();
		}

	public function set_data() {
		$this->load->helper('url');
		$data = array(
		//nama field dalam tabel 	//atribut nama dari view
			'iddsn' => $this->input->post('id'),
			'namadsn' => $this->input->post('nama'),
			'genderdsn' => $this->input->post('kelamin'),
			'tempatlahirdsn' => $this->input->post('tempatlahir'),
			'tanggallahirdsn' => $this->input->post('tanggallahir'),
			'alamatdsn' => $this->input->post('alamat'),
			'jurusandsn' => $this->input->post('jurusan'),
			'status' => "0"
			);
					//nama tabel di database  // parameter array
		return $this->db->insert('datadsn', $data);
	}

	public function update_data($id){
		$this->load->helper('url');
		$data = array(
			'iddsn' => $this->input->post('id'),
			'namadsn' => $this->input->post('nama'),
			'genderdsn' => $this->input->post('kelamin'),
			'tempatlahirdsn' => $this->input->post('tempatlahir'),
			'tanggallahirdsn' => $this->input->post('tanggallahir'),
			'alamatdsn' => $this->input->post('alamat'),
			'jurusandsn' => $this->input->post('jurusan')
			);

		$this->db->where('iddsn', $id);
						//nama tabel di database  // parameter array
		return $this->db->update('datadsn', $data);
	}

	public function delete_data($id){
		return $this->db->delete('datadsn', array('iddsn' => $id));
	}




}
 ?>
