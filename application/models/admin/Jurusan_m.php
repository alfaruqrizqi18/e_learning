<?php 

/**
* 
*/
class Jurusan_m extends CI_Model{
	
	public function __construct(){
		$this->load->database();
	}

	public function get_data($param = FALSE){

		if ($param === FALSE) {
		$query = $this->db->query('SELECT * FROM datajurusan'); //datajurusan adalah tabel database
		return $query->result_array();
		}
	}
	public function get_data_id($id){
		$query = $this->db->get_where('datajurusan', array('idjurusan' => $id )); 
									  //news adalah tabel database
									  //slug adalah field tabel
								      //$slug adalah parameter
		return $query->row_array();
	}

	public function set_data(){
		$this->load->helper('url');
		$data = array(
		//nama field dalam tabel 	//atribut nama dari view
			'idjurusan' => $this->input->post('id'),
			'namajurusan' => $this->input->post('nama'),
			'status' => "Aktif"
			);
					//nama tabel di database  // parameter array 
		return $this->db->insert('datajurusan', $data);
	}
	public function update_data($id){
		$this->load->helper('url');

		$data = array(
		//nama field dalam tabel 	//atribut nama dari view
			'namajurusan' => $this->input->post('nama'),
			'status' => $this->input->post('status')
			);
					//nama tabel di database  // parameter array 
		$this->db->where('idjurusan', $id);
		return $this->db->update('datajurusan', $data);
	}

	public function delete_data($id){
		return $this->db->delete('datajurusan', array('idjurusan' => $id));
	}

	
}
 ?>
