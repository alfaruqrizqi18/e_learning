<?php

/**
*
*/
class Mahasiswa_m extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_data() {
		$query = $this->db->query(
				'SELECT datamhs.`idmhs`,datamhs.`namamhs`,datamhs.`gendermhs`,
				 datamhs.`tempatlahirmhs`, date_format(datamhs.`tanggallahirmhs`,"%d %M %Y")
				 as tanggallahirmhs, datamhs.`alamatmhs`,datajurusan.`namajurusan`,
				 datamhs.`semestermhs`, datamhs.`kelasmhs`
                 FROM 	datamhs, datajurusan
                 WHERE 	datajurusan.`idjurusan` = datamhs.`jurusanmhs`
                 ORDER BY datajurusan.`namajurusan` ASC');
		return $query->result_array();
	}

	public function detail_data($id) {
			$data = $this->db->query(
					"SELECT datamhs.`idmhs`,datamhs.`namamhs`,datamhs.`gendermhs`,datamhs.`tempatlahirmhs`,
                    	datamhs.`tanggallahirmhs`,datamhs.`alamatmhs`, datamhs.`jurusanmhs`,
                    	datajurusan.`namajurusan`,datamhs.`semestermhs`,datamhs.`kelasmhs`
                    FROM datamhs, datajurusan
                    WHERE datamhs.`idmhs` = '$id' AND datamhs.`jurusanmhs`=datajurusan.`idjurusan`");
			return $data->result_array();
		}
	public function data_jurusan() {
			$data = $this->db->query('select * from datajurusan');
			return $data->result_array();
		}

	public function set_data() {
		$this->load->helper('url');
		$data = array(
		//nama field dalam tabel 	//atribut nama dari view
			'idmhs' => $this->input->post('id'),
			'namamhs' => $this->input->post('nama'),
			'gendermhs' => $this->input->post('kelamin'),
			'tempatlahirmhs' => $this->input->post('tempatlahir'),
			'tanggallahirmhs' => $this->input->post('tanggallahir'),
			'alamatmhs' => $this->input->post('alamat'),
			'jurusanmhs' => $this->input->post('jurusan'),
			'semestermhs' => $this->input->post('semester'),
			'kelasmhs' => $this->input->post('kelas'),
			'status' => "0"
			);
					//nama tabel di database  // parameter array
		return $this->db->insert('datamhs', $data);
	}

	public function update_data($id){
		$this->load->helper('url');
		$data = array(
			'idmhs' => $this->input->post('id'),
			'namamhs' => $this->input->post('nama'),
			'gendermhs' => $this->input->post('kelamin'),
			'tempatlahirmhs' => $this->input->post('tempatlahir'),
			'tanggallahirmhs' => $this->input->post('tanggallahir'),
			'alamatmhs' => $this->input->post('alamat'),
			'jurusanmhs' => $this->input->post('jurusan'),
			'semestermhs' => $this->input->post('semester'),
			'kelasmhs' => $this->input->post('kelas')
			);

		$this->db->where('idmhs', $id);
						//nama tabel di database  // parameter array
		return $this->db->update('datamhs', $data);
	}

	public function delete_data($id){
		return $this->db->delete('datamhs', array('idmhs' => $id));
	}




}
 ?>
