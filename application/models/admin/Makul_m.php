<?php 

/**
* 
*/
class Makul_m extends CI_Model{
	
	public function __construct(){
		$this->load->database();
	}

	public function get_data() {
		$query = $this->db->query(
				'SELECT datamakul.idmakul, datamakul.namamakul, datajurusan.namajurusan,
						datamakul.semestermakul
                 FROM datamakul, datajurusan
                 WHERE datajurusan.idjurusan = datamakul.jurusanmakul
                 ORDER BY datamakul.idmakul ASC'); 
		return $query->result_array();
	}

	public function detail_data($id) {
			$data = $this->db->query(
				"SELECT datamakul.idmakul, datamakul.namamakul, datajurusan.namajurusan,
						datamakul.semestermakul, datamakul.jurusanmakul
                 FROM datamakul, datajurusan
                 WHERE datajurusan.idjurusan = datamakul.jurusanmakul
                 AND datamakul.`idmakul` = '$id' ");
			return $data->result_array();
		}

	public function data_jurusan() {
			$data = $this->db->query('select * from datajurusan');
			return $data->result_array();
		}

	public function set_data() {
		$this->load->helper('url');
		$data = array(
		//nama field dalam tabel 	//atribut nama dari view
			'idmakul' => $this->input->post('id'),
			'namamakul' => $this->input->post('nama'),
			'jurusanmakul' => $this->input->post('jurusan'),
			'semestermakul' => $this->input->post('semester')
			);
					//nama tabel di database  // parameter array 
		return $this->db->insert('datamakul', $data);
	}

	public function update_data($id){
		$this->load->helper('url');
		$data = array(
			'idmakul' => $this->input->post('id'),
			'namamakul' => $this->input->post('nama'),
			'jurusanmakul' => $this->input->post('jurusan'),
			'semestermakul' => $this->input->post('semester')
			);
					
		$this->db->where('idmakul', $id);
						//nama tabel di database  // parameter array 
		return $this->db->update('datamakul', $data);
	}

	public function delete_data($id){
		return $this->db->delete('datamakul', array('idmakul' => $id));
	}

	

    
}
 ?>
