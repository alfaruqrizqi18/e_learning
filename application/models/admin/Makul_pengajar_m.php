<?php

/**
*
*/
class Makul_pengajar_m extends CI_Model{

	public function __construct(){
		$this->load->database();
	}

	public function get_data() {
		$query = $this->db->query(
    "SELECT datamakulpengajar.`idmakulpengajar`, datadsn.`namadsn`,
						datamakul.`namamakul`,datajurusan.`namajurusan`
     FROM datadsn, datamakul, datajurusan, datamakulpengajar
     WHERE datamakul.`idmakul` = datamakulpengajar.`namamakul`
     AND datadsn.`iddsn` = datamakulpengajar.`namapengajar`
     AND datajurusan.`idjurusan` = datamakul.`jurusanmakul`
		 ORDER BY namadsn ASC");
		return $query->result_array();
	}

	public function data_dsn() {
			$data = $this->db->query('SELECT * FROM datadsn');
			return $data->result_array();
		}

	public function get_makul($iddsn){
		$makul="<option value='0'>--Pilih Mata Kuliah--</pilih>";
		$mak= $this->db->query(
					"SELECT datamakul.`idmakul`,datamakul.`namamakul`, datadsn.`namadsn`
						FROM datamakul, datadsn
						WHERE datamakul.`jurusanmakul` = datadsn.`jurusandsn`
						AND datadsn.`iddsn` = '$iddsn'");
		foreach ($mak->result_array() as $data ){
		$makul.= "<option value='$data[idmakul]'>$data[namamakul]</option>";
		}
		return $makul;
		}

	public function detail_data($id) {
			$data = $this->db->query(
					"SELECT datamakulpengajar.`idmakulpengajar`, datadsn.`iddsn`,
					 datadsn.`namadsn`, datamakul.`namamakul`
					 FROM datadsn, datamakul, datamakulpengajar
					 WHERE datadsn.`iddsn` = datamakulpengajar.`namapengajar`
					 AND datamakul.`idmakul` = datamakulpengajar.`namamakul`
					 AND datamakulpengajar.`idmakulpengajar` = '$id'");
			return $data->result_array();
		}

//     //////////////////////////////////////////////////////////////
//     //////////////////////////////////////////////////////////////
	public function set_data() {
		$this->load->helper('url');
		$data = array(
		//nama field dalam tabel 	//atribut nama dari view
			'namapengajar' => $this->input->post('nama'),
			'namamakul' => $this->input->post('makul')
			);
					//nama tabel di database  // parameter array
		return $this->db->insert('datamakulpengajar', $data);
	}

	public function update_data($id){
		$this->load->helper('url');
		$data = array(
			'idmakulpengajar' =>$this->input->post('id'),
      'namapengajar' => $this->input->post('nama'),
			'namamakul' => $this->input->post('makul')
			);

		$this->db->where('idmakulpengajar', $id);
						//nama tabel di database  // parameter array
		return $this->db->update('datamakulpengajar', $data);
	}
//   //////////////////////////////////////////////////////////////
//   //////////////////////////////////////////////////////////////
	public function delete_data($id){
		return $this->db->delete('datamakulpengajar', array('idmakulpengajar' => $id));
	}




}
 ?>
