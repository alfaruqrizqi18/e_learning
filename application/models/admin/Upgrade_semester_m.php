<?php

/**
 *
 */
class Upgrade_semester_m extends CI_Model{

  function __construct(){
    $this->load->database();
  }

  function upgrade(){
    $query = $this->db->query(
             "SELECT idmhs, semestermhs FROM datamhs");
    foreach ($query->result() as $data) {
      $idmhs = $data->idmhs;
      $semestermhs = $data->semestermhs;
      $upgrade_semester = $semestermhs + 1;
      $array = array(
        'semestermhs' => $upgrade_semester
  			);
      $this->db->where('idmhs', $idmhs);
      $this->db->update('datamhs', $array);
    }
    redirect('mahasiswa');
  }

}


 ?>
