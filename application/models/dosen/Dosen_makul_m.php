<?php

/**
 *
 */
class Dosen_makul_m extends CI_Model
{

  public function __construct(){
		$this->load->database();
      $this->load->helper(array('url','form'));
	}


  //GET DATA UNTUK LIST MAKUL DI HEADER///
  public function get_data() {
    $id = $this->session->userdata('namaakun');
		$query = $this->db->query(
				"SELECT datamakul.`idmakul`, datamakul.`namamakul`, datadsn.`namadsn`
         FROM datamakul, datamakulpengajar, datadsn
         WHERE datamakul.`idmakul` = datamakulpengajar.`namamakul`
         AND datadsn.`iddsn` = datamakulpengajar.`namapengajar`
         AND datamakulpengajar.`namapengajar` = '$id'");
		return $query->result_array();
	}

  public function get_id_makul($id){
    $query = $this->db->query("SELECT idmakul FROM datamakul
                               WHERE idmakul = '$id' GROUP BY idmakul");
    return $query->result_array();
  }

  //SHOW ALL MATERI UNTUK TAMPIL SEMUA DATA MATERI DOSEN///
  public function show_all_materi($id){
    $query = $this->db->query("SELECT * FROM datamateri
                               WHERE namamakul = '$id'");
    return $query->result_array();
  }

  public function show_count($id){
    $query = $this->db->query("SELECT count(*) AS hitung, idmateri, namamateri, tanggal
                               FROM datamateri
                               WHERE namamakul = '$id'");
    return $query->result_array();
  }
  public function show_nama_makul($id){
    $query = $this->db->query("SELECT * from datamakul where idmakul = '$id'");
    return $query->result_array();
  }

  public function set_data(){
		$id = $_POST['id'];
    $namamateri = $_POST['nama'];
    $namapengajar = $this->session->userdata('namaakun');
    $tgl = date("d-M-Y");
    $namafile= $_FILES['materi']['name'];
    $sizefile= $_FILES['materi']['size'];
    $typefile= $_FILES['materi']['type'];
    $url = "materi/".$namafile;

    $data = array(
      'namamakul' => $id,
      'namamateri' => $namamateri,
      'namapengajar' => $namapengajar,
      'tanggal' => $tgl,
      'namafile' => $namafile,
      'size' => $sizefile,
      'type' => $typefile,
      'dir' => $url
      );
					//nama tabel di database  // parameter array
        if (move_uploaded_file($_FILES["materi"]["tmp_name"], $url)) {
            $query = $this->db->insert('datamateri', $data);
        }else{
          redirect('single_makul/index/'.$id);
        }

  }

  public function delete_data($id){
    return $this->db->delete('datamateri', array('idmateri' => $id));
  }

}

 ?>
