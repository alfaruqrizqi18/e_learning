<?php

/**
 *
 */
class Mahasiswa_makul_m extends CI_Model
{

  public function __construct(){
		$this->load->database();
	}

  //GET DATA UNTUK TAMPIL LIST MAKUL SAMPING//
  public function get_data() {
    $id = $this->session->userdata('namaakun');
    $semestermhs = $this->session->userdata('semestermhs');
    $jurusanmhs = $this->session->userdata('jurusanmhs');
		$query = $this->db->query(
				"SELECT datamakul.`idmakul`,datamhs.`namamhs`, datamakul.`namamakul`
                                 FROM datamhs, datamakul
                                 WHERE datamakul.`semestermakul` = '$semestermhs'
                                 AND datamhs.`semestermhs` = '$semestermhs'
                                 AND datamhs.`idmhs` = '$id'
                                 AND datamhs.`jurusanmhs` = '$jurusanmhs'
                                 AND datamakul.`jurusanmakul` ='$jurusanmhs'");
		return $query->result_array();
	}

  //SHOW ALL MATERI UNTUK TAMPIL SEMUA DATA MATERI DOSEN///
  public function show_all_materi($id){
    $query = $this->db->query(
        "SELECT datamateri.`idmateri`,datamakul.`idmakul`,datamateri.`namamateri`,
                datamateri.`tanggal`, datadsn.`namadsn`, datamateri.`namafile`
                             FROM datamateri, datamakul, datadsn
                             WHERE datamateri.`namamakul` = '$id'
                             AND datamakul.`idmakul` = '$id'
                             AND datadsn.`iddsn` = datamateri.`namapengajar`");
    return $query->result_array();
  }

  public function show_count($id){
    $query = $this->db->query("SELECT count(*) AS hitung, idmateri, namamateri, tanggal
                               FROM datamateri
                               WHERE namamakul = '$id'");
    return $query->result_array();
  }
  public function show_nama_makul($id){
    $query = $this->db->query("SELECT * from datamakul where idmakul = '$id'");
    return $query->result_array();
  }

}

 ?>
