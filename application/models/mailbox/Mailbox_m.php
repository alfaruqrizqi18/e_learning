<?php

/**
 *
 */
class Mailbox_m extends CI_Model{

  function __construct(){
    $this->load->database();
  }

  public function get_data(){
    $id = $this->session->userdata('namaakun');
    if ($this->session->userdata("jabatan")=="Dosen") {
      $query = $this->db->query(
                        "SELECT datapesan.idpesan, datapesan.judulpesan, datapesan.status,
                                datamhs.namamhs, datapesan.tglpesan
                         FROM datapesan, datamhs
                         WHERE datapesan.pengirim = datamhs.idmhs
                         AND datapesan.penerima = '$id'
                         ORDER BY datapesan.idpesan DESC"); //datajurusan adalah tabel database
  		return $query->result_array();

    }else if($this->session->userdata("jabatan")=="Mahasiswa"){

      $query = $this->db->query(
                        "SELECT datapesan.idpesan, datapesan.judulpesan, datapesan.status,
                                datadsn.namadsn, datapesan.tglpesan
                         FROM datapesan, datadsn
                         WHERE datapesan.pengirim = datadsn.iddsn
                         AND datapesan.penerima = '$id'
                         ORDER BY datapesan.idpesan DESC"); //datajurusan adalah tabel database
  		return $query->result_array();
    }
  }

  public function count_get_data_unread(){
    $id = $this->session->userdata('namaakun');
    if ($this->session->userdata("jabatan")=="Dosen") {
      $query = $this->db->query(
                        "SELECT count(*) as jumlah_unread
                         FROM datapesan, datamhs
                         WHERE datapesan.pengirim = datamhs.idmhs
                         AND datapesan.penerima = '$id'
                         AND datapesan.status = 'Unread'");
  		return $query->result_array();
    }else if($this->session->userdata("jabatan")=="Mahasiswa"){
      $query = $this->db->query(
                        "SELECT count(*) as jumlah_unread
                         FROM datapesan, datadsn
                         WHERE datapesan.pengirim = datadsn.iddsn
                         AND datapesan.penerima = '$id'
                         AND datapesan.status = 'Unread'");
  		return $query->result_array();
    }
  }

  public function show_detail_mail($id){
    if ($this->session->userdata("jabatan")=="Dosen") {
      $query = $this->db->query(
                      "SELECT datamhs.idmhs, datamhs.namamhs, datapesan.idpesan, datapesan.judulpesan,
                              datapesan.isipesan, datapesan.tglpesan
                       FROM datamhs, datapesan
                       WHERE datapesan.pengirim = datamhs.idmhs
                       AND datapesan.idpesan = '$id'");
    }else if($this->session->userdata("jabatan")=="Mahasiswa"){
      $query = $this->db->query(
                      "SELECT datadsn.iddsn, datadsn.namadsn, datapesan.idpesan, datapesan.judulpesan,
                              datapesan.isipesan, datapesan.tglpesan
                       FROM datadsn, datapesan
                       WHERE datapesan.pengirim = datadsn.iddsn
                       AND datapesan.idpesan = '$id'");
    }

		return $query->result_array();
	}

  public function set_data(){
    $this->load->helper('url');
    $pengirim = $this->session->userdata("namaakun");
    if ($this->session->userdata("jabatan")=="Dosen") {
      $tgl = date("d-M-y");
  		$data = array(
  		//nama field dalam tabel 	//atribut nama dari view
  			'pengirim' => $pengirim,
  			'penerima' => $this->input->post('penerima'),
  			'judulpesan' => $this->input->post('judulpesan'),
  			'isipesan' => $this->input->post('isipesan'),
        'tglpesan' => $tgl,
  			'status' => "unread"
  			);
      return $this->db->insert('datapesan', $data);
      } else if ($this->session->userdata("jabatan")=="Mahasiswa") {
        $tgl = date("d-M-y");
    		$data = array(
    		//nama field dalam tabel 	//atribut nama dari view
    			'pengirim' => $pengirim,
    			'penerima' => $this->input->post('penerima'),
    			'judulpesan' => $this->input->post('judulpesan'),
    			'isipesan' => $this->input->post('isipesan'),
          'tglpesan' => $tgl,
    			'status' => "unread"
    			);
        return $this->db->insert('datapesan', $data);
      }
    }

    public function update_status($id){
  		$this->load->helper('url');
  		$data = array(
  			'status' => "read"
  			);
  		$this->db->where('idpesan', $id);
  		return $this->db->update('datapesan', $data);
  	}

    public function reply($id){
      $pengirim = $this->session->userdata("namaakun");
      $tgl = date("d-M-y");
      if ($this->session->userdata("jabatan")=="Dosen") {
    		$data = array(
    		//nama field dalam tabel 	//atribut nama dari view
    			'pengirim' => $pengirim,
    			'penerima' => $this->input->post('id'),
    			'judulpesan' => $this->input->post('judulpesan'),
    			'isipesan' => $this->input->post('isipesan'),
          'tglpesan' => $tgl,
    			'status' => "unread"
    			);
        return $this->db->insert('datapesan', $data);
        } else if ($this->session->userdata("jabatan")=="Mahasiswa") {
      		$data = array(
      		//nama field dalam tabel 	//atribut nama dari view
          'pengirim' => $pengirim,
          'penerima' => $this->input->post('id'),
          'judulpesan' => $this->input->post('judulpesan'),
          'isipesan' => $this->input->post('isipesan'),
          'tglpesan' => $tgl,
          'status' => "unread"
      			);
          return $this->db->insert('datapesan', $data);
        }
    }

    public function delete_data($id){
  		return $this->db->delete('datapesan', array('idpesan' => $id));
  	}

  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  public function data_dosen(){
    $jurusan = $this->session->userdata('jurusanmhs');
    $query = $this->db->query("SELECT datadsn.iddsn, datadsn.namadsn
                               FROM datadsn
                               WHERE datadsn.jurusandsn = '$jurusan'");
    return $query->result_array();
  }
  public function data_dosen_get_id_reply($id){
    $query = $this->db->query("SELECT datadsn.iddsn, datadsn.namadsn
                               FROM datadsn
                               WHERE datadsn.iddsn = '$id'");
    return $query->result_array();
  }
  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  public function data_mahasiswa(){
    $jurusan = $this->session->userdata('jurusandsn');
    $query = $this->db->query("SELECT datamhs.idmhs, datamhs.namamhs
                               FROM datamhs
                               WHERE datamhs.jurusanmhs = '$jurusan'");
    return $query->result_array();
  }
  public function data_mahasiswa_get_id_reply($id){
    $query = $this->db->query("SELECT datamhs.idmhs, datamhs.namamhs
                               FROM datamhs
                               WHERE datamhs.idmhs = '$id'");
    return $query->result_array();
  }
  ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////



}
 ?>
