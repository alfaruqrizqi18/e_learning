<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Form Tambah Akun Mahasiswa
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Akun Mahasiswa</h3>
      </div>
      <div class="box box-primary">
          <!-- form start -->
          <form action="<?php echo base_url('akun_mhs/create') ?>" method="POST" role="form">
            <div class="box-body">
              <div class="form-group">
                <label>Nama Pemilik</label>
                  <select name="nama" class="form-control select2"  style="width: 100%;">
                    <?php foreach ($data as $item) { ?>
                    <option value="<?php echo $item['idmhs']; ?>"><?php echo $item['namamhs']; ?></option>
                    <?php }?>
                  </select>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Username</label>
                <input name="username" type="text" class="form-control" id="exampleInputPassword1" placeholder="Isikan Username disini..">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Isikan Password disini..">
              </div>
              </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
            </div>
          </form>
        </div>
    </div><!-- /.box -->

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
