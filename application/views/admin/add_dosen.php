<?php echo validation_errors(); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Tambah Dosen
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Dosen</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box box-primary">
                <!-- form start -->
                <form action="<?php echo base_url('dosen/create'); ?>" method="POST" role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">No. Induk Dosen</label>
                      <input name="id" type="text" class="form-control" id="exampleInputEmail1" placeholder="Contoh : 129992039">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Dosen</label>
                      <input name="nama" type="text" class="form-control" id="exampleInputPassword1" placeholder="Contoh : Kunti Elliyen">
                    </div>
                    <div class="form-group">
                      <label>Pilih Kelamin</label>
                      <select name="kelamin" class="form-control">
                        <option>Pria</option>
                        <option>Wanita</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tempat Lahir</label>
                      <input name="tempatlahir" type="text" class="form-control" id="exampleInputPassword1" placeholder="Contoh : Kediri">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tanggal Lahir</label>
                      <input name="tanggallahir" type="date" class="form-control" id="exampleInputPassword1" placeholder="Contoh : 12 Desember 2015">
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea name="alamat" class="form-control" rows="3" placeholder="Contoh : Perum. Doko Indah C-31, Kediri"></textarea>
                    </div>
                    <div class="form-group">
                      <label>Jurusan</label>
                        <select class="form-control" name="jurusan">
                          <?php foreach ($data as $item) { ?>
                          <option value="<?php echo $item['idjurusan']; ?>"><?php echo $item['namajurusan']; ?></option>
                          <?php }?>
                        </select>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
                  </div>
                </form>
              </div>
          </div><!-- /.box -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
