<?php echo validation_errors(); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Tambah Jurusan
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Jurusan</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box box-primary">
                <!-- form start -->
                <?php echo form_open('jurusan/create'); ?>
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">ID Jurusan</label>
                      <input name="id" type="text" class="form-control" id="exampleInputEmail1" placeholder="Isikan ID Jurusan disini..">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Jurusan</label>
                      <input name="nama" type="text" class="form-control" id="exampleInputPassword1" placeholder="Isikan Nama Jurusan disini..">
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
                  </div>
                <?php echo form_close(); ?>
              </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
