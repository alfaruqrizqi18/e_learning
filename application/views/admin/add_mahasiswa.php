<?php echo validation_errors(); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Tambah Mahasiswa
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Dosen</h3>
            </div>
            <div class="box box-primary">
                <!-- form start -->
                <?php echo form_open('mahasiswa/create'); ?>
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">NIM</label>
                      <input name="id" type="text" class="form-control" id="exampleInputEmail1" placeholder="Contoh : 129992039">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Mahasiswa</label>
                      <input name="nama" type="text" class="form-control" id="exampleInputPassword1" placeholder="Contoh : Kunti Elliyen">
                    </div>
                    <div class="form-group">
                      <label>Pilih Kelamin</label>
                      <select name="kelamin" class="form-control">
                        <option>Pria</option>
                        <option>Wanita</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tempat Lahir</label>
                      <input name="tempatlahir" type="text" class="form-control" id="exampleInputPassword1" placeholder="Contoh : Kediri">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tanggal Lahir</label>
                      <input name="tanggallahir" type="date" class="form-control" id="exampleInputPassword1" placeholder="Contoh : 12 Desember 2015">
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea name="alamat" class="form-control" rows="3" placeholder="Contoh : Perum. Doko Indah C-31, Kediri"></textarea>
                    </div>
                    <div class="form-group">
                      <label>Jurusan</label>
                        <select class="form-control" name="jurusan">
                          <?php foreach ($data as $item) { ?>
                          <option value="<?php echo $item['idjurusan']; ?>"><?php echo $item['namajurusan']; ?></option>
                          <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label>Pilih Semester</label>
                      <select name="semester" class="form-control">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Pilih Kelas</label>
                      <select name="kelas" class="form-control">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                        <option>E</option>
                        <option>F</option>
                        <option>G</option>
                        <option>H</option>
                        <option>I</option>
                      </select>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
                  </div>
                 <?php echo form_close(); ?>
              </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
