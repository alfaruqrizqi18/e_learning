<?php echo validation_errors(); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Tambah Mata Kuliah
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Mata Kuliah</h3>
            </div>
            <div class="box box-primary">
                <!-- form start -->
                <form action="<?php echo base_url('makul/create'); ?>" method="POST" role="form">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">ID Mata Kuliah</label>
                      <input name="id" type="text" class="form-control" id="exampleInputEmail1" placeholder="Contoh : TI001">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Mata Kuliah</label>
                      <input name="nama" type="text" class="form-control" id="exampleInputPassword1" placeholder="Contoh : Pemrograman Web 1">
                    </div>
                    <div class="form-group">
                      <label>Jurusan</label>
                        <select class="form-control" name="jurusan">
                          <?php foreach ($data as $item) { ?>
                          <option value="<?php echo $item['idjurusan']; ?>"><?php echo $item['namajurusan']; ?></option>
                          <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                      <label>Pilih Semester</label>
                      <select name="semester" class="form-control">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                      </select>
                    </div>
                    </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
                  </div>
                </form>
              </div>
          </div><!-- /.box -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
