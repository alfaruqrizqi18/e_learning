<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Form Tambah Makul-Pengajar
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Makul-Pengajar</h3>
      </div>
      <div class="box box-primary">
          <!-- form start -->
          <form action="<?php echo base_url('makul_pengajar/create') ?>" method="POST" role="form">
            <div class="box-body">
            <div class="form-group">
              <label>Nama Pengajar</label>
              <select name="nama" id="nama" class="form-control select2"
               style="width: 100%;">
               <option value="0" selected="selected">-- Pilih Pengajar --</option>
               <?php foreach ($data as $item) { ?>
                  <option value="<?php echo $item['iddsn']; ?>"><?php echo $item['namadsn']; ?></option>
               <?php }?>
                ?>
              </select>
            </div>
            <div class="form-group">
              <label>Mata Kuliah</label>
              <select name="makul" id="makul" class="form-control select2" style="width: 100%;">
              </select>
            </div>
              </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
            </div>
          </form>
        </div>
    </div><!-- /.box -->

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
