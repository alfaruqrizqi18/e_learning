<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Master Akun Mahasiswa
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
          <a href="<?php echo base_url('akun_mhs/create') ?>"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Akun Mahasiswa</button></a>
          <a href="<?php echo base_url('akun_dsn') ?>"><button class="btn btn-flat btn-sm btn-success"><i class="fa fa-eye"></i> Lihat Akun Dosen</button></a>
              <div class="box-tools">
              <form>
              <div class="input-group" style="width: 150px;">
                <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                <div class="input-group-btn">
                  <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tr>
                <th>Nama Mahasiswa</th>
                <th>Username</th>
                <th>Jabatan</th>
                <th>Operasi</th>
              </tr>
               <?php
                foreach ($dataakun as $data) {
                ?>
              <tr>
                <td><?php echo $data['namamhs']; ?></td>
                <td><?php echo $data['username']; ?></td>
                <td><?php echo $data['jabatan']; ?></td>
                <td><a href="<?php echo base_url('akun_mhs/update/'.$data['idakun']) ?>"><button class="btn btn-flat btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</button></a>
                <a href="<?php echo base_url('akun_mhs/delete/'.$data['idakun'].'/'.$data['namaakun']) ?>"><button class="btn btn-flat btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button></a></td>
              </tr>
              <?php } ?>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
