<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Master Mata Kuliah
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <a href="<?php echo base_url('makul/create'); ?>"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data Baru</button></a>
                    <div class="box-tools">
                    <form>
                    <div class="input-group" style="width: 150px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </form>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>ID Makul</th>
                      <th>Nama Makul</th>
                      <th>Jurusan</th>
                      <th>Semester</th>
                      <th>Operasi</th>
                    </tr>
                    <?php
                    foreach ($datamakul as $data) {
                    ?>
                    <tr>
                      <td><?php echo $data['idmakul']; ?></td>
                      <td><?php echo $data['namamakul']; ?></td>
                      <td><?php echo $data['namajurusan']; ?></td>
                      <td><?php echo $data['semestermakul']; ?></td>
                      <td><a href="<?php echo base_url('makul/update/'.$data['idmakul']); ?>"><button class="btn btn-flat btn-sm btn-warning"><i class="fa fa-edit"></i> Edit</button></a>
                      <a href="<?php echo base_url('makul/delete/'.$data['idmakul']); ?>"><button class="btn btn-flat btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button></a></td>
                    </tr>
                    <?php } ?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
