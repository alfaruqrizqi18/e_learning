<?php echo validation_errors(); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Form Tambah Akun Mahasiswa
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Akun Mahasiswa</h3>
      </div>
      <div class="box box-primary">
          <!-- form start -->
          <?php foreach ($data1 as $data) { ?>
          <form action="<?php echo base_url('akun_dsn/update/'.$data['idakun']) ?>" method="POST" role="form">
            <div class="box-body">
              <input name="username" type="hidden" value="<?php echo $data['idakun'] ?>">
            <div class="form-group">
              <label>Nama Pemilik</label>
              <select class="form-control select2 " readonly="true" name="nama">
                <option value="<?php echo $data['namaakun'] ?>"><?php echo "Pemilik Akun : ".$data['namadsn'] ?></option>
              </select>
            </div>
            <div class="form-group">
              <label>Jabatan</label>
              <select class="form-control select2 "  name="jabatan">
                <option value="<?php echo $data['jabatan'] ?>"><?php echo "Jabatan Akun : ".$data['jabatan'] ?></option>
                <option value="<?php echo $data['jabatan'] ?>">-- Pilih Jabatan --</option>
                <option value="Dosen">Dosen</option>
                <option value="Admin">Admin</option>
              </select>
            </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Username</label>
                <input name="username" type="text" class="form-control" id="exampleInputPassword1"
                placeholder="Isikan Username disini.." value="<?php echo $data['username'] ?>">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input name="password" type="password" class="form-control" id="exampleInputPassword1"
                placeholder="Isikan Password disini.." value="<?php echo $data['password'] ?>">
              </div>
              </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
            </div>
          </form>
          <?php } ?>
        </div>
    </div><!-- /.box -->

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
