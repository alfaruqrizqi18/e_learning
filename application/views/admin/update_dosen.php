<?php echo validation_errors(); ?>
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Edit Dosen
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Dosen</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box box-primary">
                <!-- form start -->
                <?php foreach ($data1 as $datadsn) { ?>
                <form action="<?php echo base_url('dosen/update/'.$datadsn['iddsn']); ?>" method="POST" role="form">
                  <div class="box-body">
                  <input type="hidden" name="id" value="<?php echo $datadsn['iddsn']; ?>">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Dosen</label>
                      <input name="nama" type="text" class="form-control" id="exampleInputPassword1" placeholder="Contoh : Kunti Elliyen" value="<?php echo $datadsn['namadsn']; ?>">
                    </div>
                    <div class="form-group">
                      <label>Pilih Kelamin</label>
                      <select name="kelamin" class="form-control">
                      <option value="Pria"<?php if ($datadsn['genderdsn']=="Pria")
                      { echo "selected=\"selected\""; } ?>>Pria
                          </option>
                      <option value="Wanita"<?php if ($datadsn['genderdsn']=="Wanita")
                      { echo "selected=\"selected\""; } ?>>Wanita
                          </option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tempat Lahir</label>
                      <input name="tempatlahir" type="text" class="form-control" id="exampleInputPassword1" placeholder="Contoh : Kediri" value="<?php echo $datadsn['tempatlahirdsn']; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tanggal Lahir</label>
                      <input name="tanggallahir" type="date" class="form-control" id="exampleInputPassword1" placeholder="Contoh : 12 Desember 2015" value="<?php echo $datadsn['tanggallahirdsn']; ?>">
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea name="alamat" class="form-control" rows="3" placeholder="Contoh : Perum. Doko Indah C-31, Kediri"><?php echo $datadsn['alamatdsn']; ?></textarea>
                    </div>
                    <div class="form-group">
                      <label>Jurusan</label>
                      <select class="form-control" name="jurusan">
                        <option value="<?php echo $datadsn['jurusandsn'] ?>"><?php echo "Jurusan sekarang : ".$datadsn['namajurusan'] ?></option>
                        <option>-- Pilih Jurusan --</option>
                        <?php foreach ($data2 as $datajurusan) { ?>
                        <option value="<?php echo $datajurusan['idjurusan'] ?>"><?php echo $datajurusan['namajurusan'] ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
                  </div>
                </form>
                <?php } ?>
              </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
