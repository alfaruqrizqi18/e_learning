<?php echo validation_errors(); ?>
<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Edit Mahasiswa
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Dosen</h3>
            </div>
            <div class="box box-primary">
                <!-- form start -->

                <?php foreach ($data1 as $datamhs) { ?>
                <?php echo form_open('mahasiswa/update/'.$datamhs['idmhs']); ?>
                  <div class="box-body">
                  <input type="hidden" name="id" value="<?php echo $datamhs['idmhs']; ?>">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Mahasiswa</label>
                      <input name="nama" type="text" class="form-control" id="exampleInputPassword1"
                      placeholder="Contoh : Kunti Elliyen" value="<?php echo $datamhs['namamhs']; ?>">
                    </div>
                    <div class="form-group">
                      <label>Pilih Kelamin</label>
                      <select name="kelamin" class="form-control">
                      <option value="Pria"<?php if ($datamhs['gendermhs']=="Pria")
                      { echo "selected=\"selected\""; } ?>>Pria
                          </option>
                      <option value="Wanita"<?php if ($datamhs['gendermhs']=="Wanita")
                      { echo "selected=\"selected\""; } ?>>Wanita
                          </option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tempat Lahir</label>
                      <input name="tempatlahir" type="text" class="form-control" id="exampleInputPassword1" placeholder="Contoh : Kediri" value="<?php echo $datamhs['tempatlahirmhs']; ?>">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Tanggal Lahir</label>
                      <input name="tanggallahir" type="date" class="form-control" id="exampleInputPassword1" placeholder="Contoh : 12 Desember 2015" value="<?php echo $datamhs['tanggallahirmhs']; ?>">
                    </div>
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea name="alamat" class="form-control" rows="3" placeholder="Contoh : Perum. Doko Indah C-31, Kediri"><?php echo $datamhs['alamatmhs']; ?></textarea>
                    </div>
                    <div class="form-group">
                      <label>Jurusan</label>
                      <select class="form-control" name="jurusan">
                        <option value="<?php echo $datamhs['jurusanmhs'] ?>"><?php echo "Jurusan sekarang : ".$datamhs['namajurusan'] ?></option>
                        <option>-- Pilih Jurusan --</option>
                        <?php foreach ($data2 as $datajurusan) { ?>
                        <option value="<?php echo $datajurusan['idjurusan'] ?>"><?php echo $datajurusan['namajurusan'] ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Pilih Semester</label>
                      <select name="semester" class="form-control">
                      <option value="1"<?php if ($datamhs['semestermhs']=="1")
                      { echo "selected=\"selected\""; } ?>>1
                          </option>
                      <option value="2"<?php if ($datamhs['semestermhs']=="2")
                      { echo "selected=\"selected\""; } ?>>2
                          </option>
                      <option value="3"<?php if ($datamhs['semestermhs']=="3")
                      { echo "selected=\"selected\""; } ?>>3
                          </option>
                      <option value="4"<?php if ($datamhs['semestermhs']=="4")
                      { echo "selected=\"selected\""; } ?>>4
                          </option>
                      <option value="5"<?php if ($datamhs['semestermhs']=="5")
                      { echo "selected=\"selected\""; } ?>>5
                          </option>
                      <option value="6"<?php if ($datamhs['semestermhs']=="6")
                      { echo "selected=\"selected\""; } ?>>6
                          </option>
                      <option value="7"<?php if ($datamhs['semestermhs']=="7")
                      { echo "selected=\"selected\""; } ?>>7
                          </option>
                      <option value="8"<?php if ($datamhs['semestermhs']=="8")
                      { echo "selected=\"selected\""; } ?>>8
                          </option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Pilih Kelas</label>
                      <select name="kelas" class="form-control">
                      <option value="A"<?php if ($datamhs['kelasmhs']=="A")
                      { echo "selected=\"selected\""; } ?>>A
                          </option>
                      <option value="B"<?php if ($datamhs['kelasmhs']=="B")
                      { echo "selected=\"selected\""; } ?>>B
                          </option>
                      <option value="C"<?php if ($datamhs['kelasmhs']=="C")
                      { echo "selected=\"selected\""; } ?>>C
                          </option>
                      <option value="D"<?php if ($datamhs['kelasmhs']=="D")
                      { echo "selected=\"selected\""; } ?>>D
                          </option>
                      <option value="D"<?php if ($datamhs['kelasmhs']=="E")
                      { echo "selected=\"selected\""; } ?>>E
                          </option>
                      <option value="D"<?php if ($datamhs['kelasmhs']=="F")
                      { echo "selected=\"selected\""; } ?>>F
                          </option>
                      <option value="D"<?php if ($datamhs['kelasmhs']=="G")
                      { echo "selected=\"selected\""; } ?>>G
                          </option>
                      <option value="D"<?php if ($datamhs['kelasmhs']=="H")
                      { echo "selected=\"selected\""; } ?>>H
                          </option>
                      <option value="D"<?php if ($datamhs['kelasmhs']=="I")
                      { echo "selected=\"selected\""; } ?>>I
                          </option>
                      </select>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
                  </div>
                <?php echo form_close(); ?>
                 <?php } ?>
              </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
