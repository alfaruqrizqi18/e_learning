<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Form Edit Mata Kuliah
          </h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Mata Kuliah</h3>
            </div>
            <div class="box box-primary">
                <!-- form start -->
                <?php foreach ($data1 as $datamakul) { ?>
                <form action="<?php echo base_url('makul/update/'.$datamakul['idmakul']); ?>" method="POST" role="form">
                  <div class="box-body">
                  <input type="hidden" name="id" value="<?php echo $datamakul['idmakul']; ?>">
                    <div class="form-group">
                      <label for="exampleInputPassword1">Nama Mata Kuliah</label>
                      <input name="nama" type="text" class="form-control" id="exampleInputPassword1"
                      placeholder="Contoh : Kunti Elliyen" value="<?php echo $datamakul['namamakul']; ?>">
                    </div>
                    <div class="form-group">
                      <label>Jurusan</label>
                      <select class="form-control" name="jurusan">
                        <option value="<?php echo $datamakul['jurusanmakul'] ?>"><?php echo "Jurusan sekarang : ".$datamakul['namajurusan'] ?></option>
                        <option>-- Pilih Jurusan --</option>
                        <?php foreach ($data2 as $datajurusan) { ?>
                        <option value="<?php echo $datajurusan['idjurusan'] ?>"><?php echo $datajurusan['namajurusan'] ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Pilih Semester</label>
                      <select name="semester" class="form-control">
                      <option value="1"<?php if ($datamakul['semestermakul']=="1")
                      { echo "selected=\"selected\""; } ?>>1
                          </option>
                      <option value="2"<?php if ($datamakul['semestermakul']=="2")
                      { echo "selected=\"selected\""; } ?>>2
                          </option>
                      <option value="3"<?php if ($datamakul['semestermakul']=="3")
                      { echo "selected=\"selected\""; } ?>>3
                          </option>
                      <option value="4"<?php if ($datamakul['semestermakul']=="4")
                      { echo "selected=\"selected\""; } ?>>4
                          </option>
                      <option value="5"<?php if ($datamakul['semestermakul']=="5")
                      { echo "selected=\"selected\""; } ?>>5
                          </option>
                      <option value="6"<?php if ($datamakul['semestermakul']=="6")
                      { echo "selected=\"selected\""; } ?>>6
                          </option>
                      <option value="7"<?php if ($datamakul['semestermakul']=="7")
                      { echo "selected=\"selected\""; } ?>>7
                          </option>
                      <option value="8"<?php if ($datamakul['semestermakul']=="8")
                      { echo "selected=\"selected\""; } ?>>8
                          </option>
                      </select>
                    </div>
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
                  </div>
                </form>
                <?php } ?>
              </div>
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
