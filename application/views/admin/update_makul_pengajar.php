<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Form Update Makul-Pengajar
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Makul-Pengajar</h3>
      </div>
      <div class="box box-primary">
          <!-- form start -->
          <?php foreach ($data1 as $datamakulpengajar) {?>
          <form action="<?php echo base_url('makul_pengajar/update/'.$datamakulpengajar['idmakulpengajar']) ?>" method="POST" role="form">
            <div class="box-body">
              <input name="id" type="hidden" value="<?php echo $datamakulpengajar['idmakulpengajar'] ?>">
            <div class="form-group">
              <label>Nama Pengajar</label>
              <select name="nama" id="nama" class="form-control select2"
               style="width: 100%;">
                  <option selected="selected" value="<?php echo $datamakulpengajar['iddsn']; ?>"><?php echo $datamakulpengajar['namadsn']; ?></option>
               <option value="0" >-- Pilih Pengajar --</option>
               <?php foreach ($data2 as $item) { ?>
                  <option value="<?php echo $item['iddsn']; ?>"><?php echo $item['namadsn']; ?></option>
               <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Mata Kuliah</label>
              <select name="makul" id="makul" class="form-control select2" style="width: 100%;">
              </select>
            </div>
              </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-flat btn-primary">Simpan</button>
            </div>
          </form>
          <?php } ?>
        </div>
    </div><!-- /.box -->

  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
