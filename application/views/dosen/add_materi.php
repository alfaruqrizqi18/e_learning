<!-- Content Wrapper. Contains page content -->
<?php echo validation_errors(); ?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Form Tambah Materi
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <!-- form start -->
          <?php foreach ($datamakul_id as $data) {?>
          <form action="<?php echo base_url('single_makul/create/'.$data['idmakul']) ?>" method="POST" enctype="multipart/form-data">
            <div class="box-body">
            <input name="id" type="hidden" value="<?php echo $data['idmakul']; ?>">
              <div class="form-group">
                <label for="exampleInputPassword1">Nama Materi</label>
                <input type="text" class="form-control" id="exampleInputPassword1" placeholder="Masukkan nama materi.."
                name="nama">
              </div>
              <div class="form-group">
                <label for="exampleInputFile">File </label>
                <input type="file" id="exampleInputFile" name="materi">
              </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Materi</button>
            </div>
          </form>
          <?php } ?>
        </div><!-- /.box -->

      </div><!--/.col (left) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
