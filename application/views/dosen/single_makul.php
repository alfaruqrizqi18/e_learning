<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
   <?php
   foreach ($show_nama_makul as $data) {
  ?>
    <h1>
    <?php echo $data['namamakul']; ?>
    </h1>
  <?php } ?>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>
          <div class="info-box-content">
          <?php
          foreach ($datamateri_count as $data) {
          ?>
            <span class="info-box-text">Total Materi</span>
            <span class="info-box-number"><?php echo $data['hitung']; ?></span>
          </div><!-- /.info-box-content -->
          <?php } ?>
        </div><!-- /.info-box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <?php foreach ($datamakul_id as $data) {?>
          <a href="<?php echo base_url('single_makul/create/'.$data['idmakul']) ?>"><button class="btn btn-flat btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Materi</button></a>
          <?php } ?>
              <div class="box-tools">
              <form>
              <div class="input-group" style="width: 150px;">
                <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                <div class="input-group-btn">
                  <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tr>
                <th>Nama Materi</th>
                <th>Tanggal Unggah Materi</th>
                <th>Operasi</th>
              </tr>

              <?php
              foreach ($datamateri_list_materi as $data) {
               ?>
               <tr>
                <td><?php echo $data['namamateri']; ?></td>
                <td><?php echo $data['tanggal']; ?></td>
                <input type="hidden" name="idmakul" value="<?php ?>">
                <td><a href="<?php echo base_url('single_makul/delete/'.$data['idmateri'].'/'.$data['namamakul']) ?>">
                <button class="btn btn-flat btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</button></a></td>
              </tr>
              <?php } ?>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
