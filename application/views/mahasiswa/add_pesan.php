<?php echo validation_errors(); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Mailbox
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-3">
        <a href="<?php echo base_url('mailbox') ?>" class="btn btn-primary btn-block margin-bottom">Kembali</a>
        <div class="box box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Navigasi Mailbox</h3>
            <div class="box-tools">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li class="active"><a href="<?php echo base_url('mailbox') ?>"><i class="fa fa-inbox"></i> Inbox</a></li>
            </ul>
          </div><!-- /.box-body -->
        </div><!-- /. box -->
      </div><!-- /.col -->
      <div class="col-md-9">
        <div class="box box-primary">
          <form  action="<?php echo base_url('mailbox/create'); ?>" method="post">
          <div class="box-header with-border">
            <h3 class="box-title">Buat Pesan Baru</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="form-group">
              <label>Kirim ke :</label>
                <select name="penerima" class="form-control select2"  style="width: 100%;">
                  <option value="">-- Pilih Penerima --</option>
                  <?php foreach ($data as $item) { ?>
                  <option value="<?php echo $item['iddsn']; ?>"><?php echo $item['namadsn']; ?></option>
                  <?php }?>
                </select>
            </div>
            <div class="form-group">
              <input class="form-control" placeholder="Judul Pesan:" name="judulpesan">
            </div>
            <div class="form-group">
              <textarea id="compose-textarea" class="form-control" style="height: 300px" name="isipesan"></textarea>
            </div>
          </div><!-- /.box-body -->
          <div class="box-footer">
            <div class="pull-right">
              <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Kirim</button>
            </div>
          </div><!-- /.box-footer -->
        </div><!-- /. box -->
        </form>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
