<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
   <?php
   foreach ($show_nama_makul as $data) {
  ?>
    <h1>
    <?php echo $data['namamakul']; ?>
    </h1>
  <?php } ?>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-book"></i></span>
          <div class="info-box-content">
          <?php
          foreach ($datamateri_count as $datacount) {
          ?>
            <span class="info-box-text">Total Materi</span>
            <span class="info-box-number"><?php echo $datacount['hitung']; ?></span>
          <?php } ?>
          </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
          <p><b>Data Materi</b></p>
              <div class="box-tools">
              <form>
              <div class="input-group" style="width: 150px;">
                <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                <div class="input-group-btn">
                  <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tr>
                <th>Nama Materi</th>
                <th>Nama Dosen</th>
                <th>Tanggal Unggah Materi</th>
                <th>Operasi</th>
              </tr>
              <?php
                foreach ($datamateri_list_materi as $data) {
               ?>
              <tr>
                <td><?php echo $data['namamateri']; ?></td>
                <td><?php echo $data['namadsn']; ?></td>
                <td><?php echo $data['tanggal']; ?></td>
                <td><a href="<?php echo base_url('download/index/'.$data['idmateri']) ?>"><button class="btn btn-flat btn-sm btn-success"><i class="fa fa-download"></i> Unduh Materi</button></a></td>
              </tr>
      <?php } ?>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
