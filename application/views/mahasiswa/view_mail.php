<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Detail Mail
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-3">
        <a href="<?php echo base_url('mailbox/create') ?>" class="btn btn-primary btn-block margin-bottom">Buat Pesan</a>
        <div class="box box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Navigasi Mailbox</h3>
            <div class="box-tools">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
              <li><a href="<?php echo base_url('mailbox') ?>"><i class="fa fa-inbox"></i> Inbox</a></li>
            </ul>
          </div><!-- /.box-body -->
        </div><!-- /. box -->
      </div><!-- /.col -->
      <?php foreach ($detail_mail as $data) { ?>
      <div class="col-md-9">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Detail Mail</h3>
            <div class="box-tools pull-right">
              <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
              <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="mailbox-read-info">
              <h3><?php echo "Subjek: ".$data['judulpesan']; ?></h3>
              <h5>Dari: <?php echo $data['namadsn'];; ?> <span class="mailbox-read-time pull-right"><?php echo $data['tglpesan']; ?></span></h5>
            </div><!-- /.mailbox-read-info -->
            <div class="mailbox-read-message">
              <p><?php echo $data['isipesan']; ?></p>
            </div><!-- /.mailbox-read-message -->
          </div><!-- /.box-body -->
          <div class="box-footer">
            <div class="pull-right">
              <a href="<?php echo base_url('mailbox/reply/'.$data['iddsn']) ?>"><button class="btn btn-default"><i class="fa fa-reply"></i> Balas</button></a>
            </div>
            <a href="<?php echo base_url('mailbox/delete/'.$data['idpesan']) ?>"><button class="btn btn-default"><i class="fa fa-trash-o"></i> Hapus</button></a>
          </div><!-- /.box-footer -->

        </div><!-- /. box -->
      </div><!-- /.col -->
      <?php } ?>
    </div><!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
