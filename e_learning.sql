-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 03 Mar 2017 pada 04.15
-- Versi Server: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_learning`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dataakun`
--

CREATE TABLE `dataakun` (
  `idakun` int(11) NOT NULL,
  `namaakun` varchar(20) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` text NOT NULL,
  `jabatan` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dataakun`
--

INSERT INTO `dataakun` (`idakun`, `namaakun`, `username`, `password`, `jabatan`) VALUES
(25, '15010086', 'rizqi', 'rizqi', 'Mahasiswa'),
(35, '23', 'd', 'd', 'Dosen'),
(36, '24', 'putut', '123', 'Dosen'),
(37, '999230', 'andik', '123', 'Dosen'),
(38, '299938829', 'soeprihadi', '123', 'Dosen'),
(39, '99938840', 'kunti', '123', 'Dosen'),
(40, '8882938', 'agus', '123', 'Dosen'),
(41, '99930', 'ellya', '123', 'Dosen'),
(42, '999', 'admin', '123', 'Admin'),
(47, '2230300', 'izzah', '123', 'Dosen');

-- --------------------------------------------------------

--
-- Struktur dari tabel `datadsn`
--

CREATE TABLE `datadsn` (
  `iddsn` varchar(20) NOT NULL,
  `namadsn` varchar(40) DEFAULT NULL,
  `genderdsn` varchar(6) DEFAULT NULL,
  `tempatlahirdsn` varchar(40) DEFAULT NULL,
  `tanggallahirdsn` date DEFAULT NULL,
  `alamatdsn` text,
  `jurusandsn` varchar(10) DEFAULT NULL,
  `fotodsn` text,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datadsn`
--

INSERT INTO `datadsn` (`iddsn`, `namadsn`, `genderdsn`, `tempatlahirdsn`, `tanggallahirdsn`, `alamatdsn`, `jurusandsn`, `fotodsn`, `status`) VALUES
('2230300', 'Abidatul Izzah', 'Wanita', 'Kediri', '2016-08-10', 'Kediri', 'J001', NULL, '1'),
('24', 'Putut', 'Pria', 'kediri', '2016-08-27', 'doko', 'J002', NULL, '1'),
('299938829', 'Ahmad Soeprihadi Darmowidjoto', 'Pria', 'Kediri', '2016-08-10', 'Banaran', 'J001', '', '1'),
('8882938', 'Agustono Heriadi', 'Pria', 'Kediri', '2016-08-29', 'Tidak diketahui', 'J001', NULL, '1'),
('999', 'Dioda Admin', 'Pria', 'Kediri', '2016-08-27', 'Jl. Dandang Gendhis No. 99', 'J001', NULL, '1'),
('999230', 'Andika Kurnia', 'Pria', 'Kediri', '2016-01-05', 'Jamsaren, Kediri', 'J001', '', '1'),
('99930', 'Ellya Nurfarida', 'Wanita', 'Kediri', '2016-08-31', 'Perum. Permata Biru', 'J001', '', '1'),
('99938840', 'Kunti Elliyen', 'Wanita', 'Kediri', '2016-08-18', 'Perum. Candra Kirana', 'J001', '', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `datajurusan`
--

CREATE TABLE `datajurusan` (
  `idjurusan` varchar(10) NOT NULL,
  `namajurusan` varchar(30) DEFAULT NULL,
  `status` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datajurusan`
--

INSERT INTO `datajurusan` (`idjurusan`, `namajurusan`, `status`) VALUES
('J001', 'Teknik Informatika', 'Tidak Aktif'),
('J002', 'Perawatan & Perbaikan Mesin', 'Aktif'),
('J003', 'Akuntansi', 'Aktif'),
('J004', 'Teknik Elektro', 'Aktif'),
('J005', 'Teknik Sipil', 'Aktif'),
('J006', 'Teknik Penerbangan', 'Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `datamakul`
--

CREATE TABLE `datamakul` (
  `idmakul` varchar(10) NOT NULL,
  `namamakul` varchar(70) NOT NULL,
  `jurusanmakul` varchar(10) NOT NULL,
  `semestermakul` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datamakul`
--

INSERT INTO `datamakul` (`idmakul`, `namamakul`, `jurusanmakul`, `semestermakul`) VALUES
('MK01', 'Pemrograman Web 1', 'J001', '2'),
('MK02', 'Basis Data', 'J001', '2'),
('MK03', 'Pendidikan Pancasila (TI)', 'J001', '2'),
('MK04', 'Interaksi Manusia Komputer', 'J001', '8'),
('MK06', 'Gelombang Arus', 'J002', '2'),
('MK07', 'Aplikasi Perkantoran', 'J001', '8'),
('MK08', 'Web Design', 'J001', '1'),
('MK09', 'E-Commerce', 'J001', '8'),
('MK10', 'Aljabar', 'J003', '2'),
('MK11', 'Dasar - dasar Java', 'J001', '1'),
('MK12', 'Pemrograman Berbasis Objek', 'J001', '2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `datamakulpengajar`
--

CREATE TABLE `datamakulpengajar` (
  `idmakulpengajar` int(5) NOT NULL,
  `namapengajar` varchar(15) DEFAULT NULL,
  `namamakul` varchar(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datamakulpengajar`
--

INSERT INTO `datamakulpengajar` (`idmakulpengajar`, `namapengajar`, `namamakul`) VALUES
(1, '99938840', 'MK01'),
(2, '99938840', 'MK07'),
(10, '999230', 'MK09'),
(11, '99930', 'MK04'),
(12, '8882938', 'MK02'),
(13, '299938829', 'MK03'),
(14, '999230', 'MK08'),
(19, '2230300', 'MK12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `datamateri`
--

CREATE TABLE `datamateri` (
  `idmateri` int(11) NOT NULL,
  `namamakul` varchar(15) NOT NULL,
  `namamateri` varchar(50) NOT NULL,
  `namapengajar` varchar(20) DEFAULT NULL,
  `tanggal` text NOT NULL,
  `namafile` text NOT NULL,
  `size` text NOT NULL,
  `type` text NOT NULL,
  `dir` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datamateri`
--

INSERT INTO `datamateri` (`idmateri`, `namamakul`, `namamateri`, `namapengajar`, `tanggal`, `namafile`, `size`, `type`, `dir`) VALUES
(1, 'MK11', 'asdasd', '2230300', '31-Oct-2016', 'Buku ORKOM.pdf', '1430955', 'application/pdf', 'materi/Buku ORKOM.pdf'),
(2, 'MK12', 'ddddd', '2230300', '31-Oct-2016', '07_Input Output.ppt', '797184', 'application/vnd.ms-powerpoint', 'materi/07_Input Output.ppt'),
(3, 'MK12', 'asdasd', '2230300', '31-Oct-2016', '04 - FLOW.mp3', '5628323', 'audio/mp3', 'materi/04 - FLOW.mp3'),
(4, 'MK12', 'sd', '2230300', '01-Nov-2016', 'Oploverz_ReLife_480p_Ep01_animesave.mp4', '52157200', 'video/mp4', 'materi/Oploverz_ReLife_480p_Ep01_animesave.mp4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `datamhs`
--

CREATE TABLE `datamhs` (
  `idmhs` varchar(20) NOT NULL,
  `namamhs` text,
  `gendermhs` varchar(6) DEFAULT NULL,
  `tempatlahirmhs` varchar(40) DEFAULT NULL,
  `tanggallahirmhs` date DEFAULT NULL,
  `alamatmhs` text,
  `jurusanmhs` varchar(5) DEFAULT NULL,
  `semestermhs` int(2) DEFAULT NULL,
  `fotomhs` text,
  `kelasmhs` varchar(2) DEFAULT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datamhs`
--

INSERT INTO `datamhs` (`idmhs`, `namamhs`, `gendermhs`, `tempatlahirmhs`, `tanggallahirmhs`, `alamatmhs`, `jurusanmhs`, `semestermhs`, `fotomhs`, `kelasmhs`, `status`) VALUES
('11', 'Sumidi', 'Pria', 's', '2016-08-26', 's', 'J001', 3, NULL, 'A', 0),
('15010075', 'Moch Tauladan', 'Pria', 'Kediri', '2016-08-16', 'Bangsalsssssssss', 'J002', 3, NULL, 'B', 0),
('15010086', 'Muhammad Rizqi Alfaruq', 'Pria', 'Kediria', '2016-08-02', 'Perum. Doko Indah C-31', 'J001', 2, '', 'B', 1),
('15010088', 'Muhammad Zakki Mubarok', 'Pria', 'Kediri', '2016-05-09', 'Jl. Barito 3, Nganjuk', 'J005', 3, NULL, 'B', 0),
('15010105', 'Roni Ahmadi', 'Pria', 'Kediri', '2016-04-11', 'Dusun Jabang 2', 'J001', 3, '', 'B', 0),
('15010106', 'Rudiath Yuwantaraa', 'Pria', 'Kediri', '2016-08-31', 'Kediri', 'J005', 3, NULL, 'B', 0),
('15015086', 'Rizqi Alfaruq', 'Pria', 'Kediri', '2016-08-24', 'Doko Indah C-31', 'J001', 3, NULL, 'A', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `datapesan`
--

CREATE TABLE `datapesan` (
  `idpesan` int(8) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `penerima` int(11) NOT NULL,
  `judulpesan` varchar(50) NOT NULL,
  `isipesan` text NOT NULL,
  `tglpesan` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datapesan`
--

INSERT INTO `datapesan` (`idpesan`, `pengirim`, `penerima`, `judulpesan`, `isipesan`, `tglpesan`, `status`) VALUES
(1, 15010086, 2230300, 'Test', 'assasdasd', '01-Nov-16', 'read'),
(2, 2230300, 15010086, 'nmas bcnmac', 'ajsmjnbcxjmsab casc', '01-Nov-16', 'read'),
(3, 15010086, 2230300, 'fdgf', 'gfhgh', '01-Nov-16', 'read'),
(4, 15010086, 2230300, 'fdhf', 'gfhgf', '01-Nov-16', 'read'),
(5, 15010086, 2230300, 'jhgjhg', 'jhghjgj', '01-Nov-16', 'read');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dataakun`
--
ALTER TABLE `dataakun`
  ADD PRIMARY KEY (`idakun`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `datadsn`
--
ALTER TABLE `datadsn`
  ADD PRIMARY KEY (`iddsn`);

--
-- Indexes for table `datajurusan`
--
ALTER TABLE `datajurusan`
  ADD PRIMARY KEY (`idjurusan`);

--
-- Indexes for table `datamakul`
--
ALTER TABLE `datamakul`
  ADD PRIMARY KEY (`idmakul`);

--
-- Indexes for table `datamakulpengajar`
--
ALTER TABLE `datamakulpengajar`
  ADD PRIMARY KEY (`idmakulpengajar`);

--
-- Indexes for table `datamateri`
--
ALTER TABLE `datamateri`
  ADD PRIMARY KEY (`idmateri`);

--
-- Indexes for table `datamhs`
--
ALTER TABLE `datamhs`
  ADD PRIMARY KEY (`idmhs`);

--
-- Indexes for table `datapesan`
--
ALTER TABLE `datapesan`
  ADD PRIMARY KEY (`idpesan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dataakun`
--
ALTER TABLE `dataakun`
  MODIFY `idakun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `datamakulpengajar`
--
ALTER TABLE `datamakulpengajar`
  MODIFY `idmakulpengajar` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `datamateri`
--
ALTER TABLE `datamateri`
  MODIFY `idmateri` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `datapesan`
--
ALTER TABLE `datapesan`
  MODIFY `idpesan` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
